#!/usr/bin/python
import MySQLdb;
import sys,getopt;
import ConfigParser

config = ConfigParser.RawConfigParser()
config.read('db.conf')
IP=config.get("mysql","IP")
USER_VIEW=config.get("mysql","USER_VIEW")
USER_VIEW_PASSWORD=config.get("mysql","USER_VIEW_PASSWORD")
DB=config.get("mysql","DB")
connection = MySQLdb.connect (host = IP, user = USER_VIEW, passwd = USER_VIEW_PASSWORD, db = DB)
cursor = connection.cursor ()


def main(argv):
   cname = ''
   try:
      opts, args = getopt.getopt(argv,"hc:",["cityn="])
   except getopt.GetoptError:
      print 'cwraper.py -c <city name> '
      sys.exit(2)
   for opt, arg in opts:
      if opt == '-h':
         print 'cwraper.py -c <city name>'
         sys.exit()
      elif opt in ("-c", "--cityn"):
         cname = arg
   	 print 'Entered city name is: ',cname 
	 statement = "SELECT c.name AS city,c.iso_code AS CITY_CODE ,r.name AS region,r.iso_code AS REGION_CODE,co.name AS country , co.alpha2 AS alpha2, co.alpha3 as alpha3, co.targetable as targetable FROM cities c,regions r,countries co WHERE c.name like '%"+cname+"%' and c.region_id = r.id and c.country_id=co.id;"
	 print statement;
	 cursor.execute(statement)
	 print "Starting loop"
	 row = cursor.fetchone()
	 while row is not None:
    		print "------------------------------------------------------------------------------"
		print "CITY , CITY_CODE , REGION , REGION_CODE , COUNTRY , ALPHA2, ALPHA3, TARGETABLE"
    		print "------------------------------------------------------------------------------"
		print ", ".join([str(c) for c in row])
    		row = cursor.fetchone()

if __name__ == "__main__":
   main(sys.argv[1:])
cursor.close()
connection.close()


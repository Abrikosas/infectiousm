#!/usr/bin/python
import MySQLdb;
import ConfigParser

config = ConfigParser.RawConfigParser()
config.read('db.conf')
IP=config.get("mysql","IP")
ROOT_USER=config.get("mysql","ROOT_USER")
ROOT_PASSWORD=config.get("mysql","ROOT_PASSWORD")
DB=config.get("mysql","DB")


connection = MySQLdb.connect (host = IP, user = ROOT_USER, passwd = ROOT_PASSWORD, db = DB)
cursor = connection.cursor ()


TABLES = {}

TABLES['cities'] = (
    "CREATE TABLE `cities` ( 	"
    "    id INT NOT NULL	,"
    "    country_id INT NOT NULL,"
    "    region_id  INT NULL	,"
    "    name VARCHAR(255) NOT NULL,"
    "    iso_code VARCHAR(255),"
    "    PRIMARY KEY (id)"
") CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB")
    

TABLES['countries'] = (
    "CREATE TABLE `countries` (    "
    "    id INT NOT NULL        ,"
    "    alpha2 VARCHAR(255) NOT NULL,"
    "    alpha3 VARCHAR(255) NOT NULL,"
    "    name VARCHAR(255),"
    "    targetable INT NOT NULL        ,"
    "    PRIMARY KEY (id)"
") ENGINE=InnoDB")


TABLES['regions'] = (
    "CREATE TABLE `regions` (    "
    "    id INT NOT NULL        ,"
    "    country_id INT NOT NULL        ,"
    "    name VARCHAR(255) NOT NULL,"
    "    iso_code VARCHAR(255) NOT NULL,"
    "    PRIMARY KEY (id)"
") ENGINE=InnoDB")

for name, ddl in TABLES.iteritems():
    try:
        print("Creating table {}: ".format(name))
        cursor.execute(ddl)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)
    else:
        print("OK")

cursor.close()
connection.close()

#!/usr/bin/python
import MySQLdb;
import json;
import csv
import ConfigParser

config = ConfigParser.RawConfigParser()
config.read('db.conf')
IP=config.get("mysql","IP")
ROOT_USER=config.get("mysql","ROOT_USER")
ROOT_PASSWORD=config.get("mysql","ROOT_PASSWORD")
DB=config.get("mysql","DB")


connection = MySQLdb.connect (host = IP, user = ROOT_USER, passwd = ROOT_PASSWORD, db = DB)

cursor = connection.cursor ()
with open('countries','rb') as c:
        country=csv.DictReader(c,delimiter=',')
        for row in country:
                name = row["name"]
                name=name.replace("'","''")
                statement = "INSERT INTO countries(id,alpha2,alpha3,name,targetable) VALUES ('"+row['id']+"','" + row['alpha2']+"','" + row['alpha3']+"','"+name + "','"+row['targetable']+"');"
                cursor.execute(statement)
connection.commit()
print "Data to table countries loaded."
		
with open('regions.csv','rb') as r:
        region=csv.DictReader(r,delimiter=',')
        for row in region:
                name = row["name"]
                name=name.replace("'","''")
                statement = "INSERT INTO regions(id,country_id,name,iso_code) VALUES ('"+row['id']+"','" + row['country_id']+"','" + name+"','" + row['iso_code']+"');"
		cursor.execute(statement)
	
connection.commit()
print "Data to table regions loaded."

with open('cities','rb') as f:
    for line in f:
	j_content = json.loads(line)
	name = j_content['name']
	name=name.replace("'","''")
        if 'region_id' in j_content:
                statement = "INSERT INTO cities (id,country_id,region_id,name,iso_code) VALUES ('"+j_content['id']+"','" + j_content['country_id']+"','" + j_content['region_id']+"','" + name+"','" + j_content['iso_code']+"');"
	
		cursor.execute(statement)
        else:
                statement = "INSERT INTO cities (id,country_id,name,iso_code) VALUES ('"+j_content['id'] +"','"+ j_content['country_id'] +"','"+ name+"','" + j_content['iso_code']+"');"
		cursor.execute(statement)
connection.commit()
print "Data to table cities loaded."

cursor.close()
connection.close()
